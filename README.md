# mcalc.zsh

```shell
% mcalc.zsh

Minute Calculator, Given time - Current time
 - dependency : % apt install dateutils

Usage:
% /Users/x/bin/mcalc.zsh [ 15:50 | 15:50:00 | 2020-03-06T15:50:00 ]
```

## ex)

```shell
% LANG=C date
Fri Mar  6 15:55:17 KST 2020

% mcalc.zsh 2020-03-08T15:50:00
2874m

% mcalc.zsh 16:50:00
53m

% mcalc.zsh 16:50
53m
```
