#!/usr/bin/env zsh

TODAY=$(date "+%Y-%m-%d")
CURRENT=$(date "+%Y-%m-%dT%H:%M:%S")

if [ "$#" -ne 1 ]; then
  echo "\nUnix Timestamp Calculator, Given time - Current time\n\nUsage:\n% $0 [ 15:50 | 15:50:00 | 2020-03-06T15:50:00 ]" >&2
  exit 1
fi

GIVEN=$1

GIVENTIME=$CURRENT

if [[ $GIVEN == *T* ]]; then
    GIVENTIME=$GIVEN
else
    a=("${(@s/:/)GIVEN}")
    # echo $a
    ac=${#a[@]}
    # echo $ac

    if [ $ac -eq 2 ]; then
        GIVENTIME=${TODAY}"T"$1":00"
    elif [ $ac -eq 3 ]; then
        GIVENTIME=${TODAY}"T"$1
    fi
fi

# datediff $CURRENT $GIVENTIME -f "%Mm"
gdate +%s -d $GIVENTIME
